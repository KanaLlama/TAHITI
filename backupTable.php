<?php
session_start();
if (!isset($_SESSION['logged_in'])) {
  $_SESSION['logged_in'] = 0;
  header('Location: login.php');
  die();
} else {
  if (!$_SESSION['logged_in'] == 1) {
    header('Location: login.php');
    die();
  }
}

function p($value='')
{
  echo "<p>";
  echo $value;
  echo "</p>";
}

function e($value='No Data')
{
  echo $value;
}

$currentDatabase = $_GET['Database'];

$sqli = mysqli_connect($_SESSION['host'], $_SESSION['user'], $_SESSION['password'], $currentDatabase);

    // Check connection
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit;
    }



?>

<?php if (!empty($_POST)): ?>
  <h1>Save Files</h1>

<?php else: ?>
  <h1>No post data</h1>
<?php endif; ?>

<!-- <a href="getTable.php">to Tables</a> -->

<?php

if (!empty($_POST)){

  $tables = array();
  $return = '';
  $fileName = 'db-backup-'.date("Y-m-d__h-m-s");
  // e('<pre>');
  // var_dump($_POST);
  // e('</pre>');

mysqli_query($sqli, "SET NAMES 'utf8'");

  if ( isset($_POST['all']) ) {
    // echo "ALL !";
    $fileName.= '-All';

    $result = mysqli_query($sqli, 'SHOW TABLES');
    while($row = mysqli_fetch_row($result))
    {
      $tables[] = $row[0];
    }

  }else {
    foreach ($_POST as $table) {
      // p($table);
      $tables[] .= $table ;
      $fileName.= '-'.$table;
    }
  }

  // var_dump($tables);

  foreach($tables as $table){


    $result = mysqli_query($sqli, 'SELECT * FROM '.$table);
    $num_fields = mysqli_num_fields($result);
    $num_rows = mysqli_num_rows($result);

    $return.= 'DROP TABLE IF EXISTS '.$table.';';
    $row2 = mysqli_fetch_row(mysqli_query($sqli, 'SHOW CREATE TABLE '.$table));
    $return.= "\n\n".$row2[1].";\n\n";
    $counter = 1;

    //Over tables
    for ($i = 0; $i < $num_fields; $i++)
    {   //Over rows
      while($row = mysqli_fetch_row($result))
      {
        if($counter == 1){
          $return.= 'INSERT INTO '.$table.' VALUES(';
        } else{
          $return.= '(';
        }

          //Over fields
        for($j=0; $j<$num_fields; $j++)
        {
          $row[$j] = addslashes($row[$j]);
          $row[$j] = str_replace("\n","\\n",$row[$j]);
          if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
          if ($j<($num_fields-1)) { $return.= ','; }
        }

        if($num_rows == $counter){
          $return.= ");\n";
        } else{
          $return.= "),\n";
        }
        ++$counter;
      }
    }
    $return.="\n\n\n";
    echo $return;
  }



  $fileName.='.sql';

  $handle = fopen("saves/".$fileName,'w+');
  fwrite($handle,$return);
  if(fclose($handle)){
    echo "<br/>";
    echo "<br/>";
    echo "<br/>";
    echo "Done, the file name is: ".$fileName;
    echo "<br/>";
    echo "<br/>";
    echo "<a href=".$fileName.">Download</a>";
    exit;
  }
  else
  {
      $tables = is_array($tables) ? $tables : explode(',',$tables);
  }

}

?>











































<!--  -->
