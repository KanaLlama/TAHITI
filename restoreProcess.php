<?php
  session_start();
  if (!isset($_SESSION['logged_in'])) {
    $_SESSION['logged_in'] = 0;
    header('Location: login.php');
    die();
  } else {
    if (!$_SESSION['logged_in'] == 1) {
      header('Location: login.php');
      die();
    }
  }

  $page = 'restore';
  include 'dist/parts/header.php';


  $currentDatabase = $_POST['Database'];

  $sqli = mysqli_connect($_SESSION['host'], $_SESSION['user'], $_SESSION['password'], $currentDatabase);

    // Check connection
    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        exit;
    }


    $errors = [];
    $lines  = [];
    $sucessFlag = false;
?>

<p>
<?php
  $handle = fopen($_FILES["save"]['tmp_name'], "r");
  if ($handle) {
    $query='';
      while (($line = fgets($handle)) !== false) {
        $line = str_replace("DEFAULT '0000-00-00 00:00:00'","DEFAULT NOW()",$line);
        $line = str_replace('"0000-00-00 00:00:00"',"NOW()",$line);
        $query.=$line;
      }



      // echo $query;
      $request = explode(';', $query);
      // var_dump($request);


      foreach ($request as $line) {
        // echo "BANANA -----------------------------------------------------------------------------------------------------------------------------------------";
        $lines[] .= $line;
        $result = mysqli_query($sqli, $line.';');
        if(!$result){
          $errors[] .= mysqli_error($sqli);
        }

        $line = null;
        $result = null;
      }
      array_pop($errors);

      $sucessFlag = true;
      fclose($handle);
  } else {
      $errors[] .= "file don't exist";
  }
?>

</p>


<?php foreach ($errors as $error): ?>
  <div class="alert alert-danger" role="alert">
    <?=$error ?>
  </div>
<?php endforeach; ?>

<?php if ($sucessFlag): ?>
  <div class="alert alert-success" role="alert">
    successfull restored
  </div>
<?php endif; ?>

<pre>
  <?php foreach ($lines as $line): ?>
    <?=$line?>
  <?php endforeach; ?>
</pre>

<?php
include 'dist/parts/footer.php';
?>
