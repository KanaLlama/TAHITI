<?php
  session_start();
  if (!isset($_SESSION['logged_in'])) {
    $_SESSION['logged_in'] = 0;
    header('Location: login.php');
    die();
  } else {
    if (!$_SESSION['logged_in'] == 1) {
      header('Location: login.php');
      die();
    }
  }

  $page = 'save';
  include 'dist/parts/header.php';

  $sqli = null;
  $sqli = mysqli_connect($_SESSION['host'], $_SESSION['user'], $_SESSION['password']);
    // or die("Impossible de se connecter : " . mysql_error());
  if (!$sqli) {
    echo '<div class="alert alert-danger" role="alert">SQL Connection Fail</div>';
    die();
  }
?>
<div class="alert alert-success" role="alert">
  SQL Connection Sucess
</div>



<?php
  $databases = $sqli->query("SHOW DATABASES");

  mysqli_close($sqli);


?>

<h1>List Databases</h1>
<?php foreach ($databases as $database): ?>
  <div class="dropdown">
    <a class="btn btn-primary" href="#" role="getTable.php?Database=<?=$database['Database']?>"><?=$database['Database']?></a>
    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
      
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
      <a class="dropdown-item" href="getTable.php?Database=<?=$database['Database']?>">Save</a>
      <a class="dropdown-item" href="restore.php?Database=<?=$database['Database']?>">Restore</a>
    </div>
  </div>
<br>
<?php endforeach; ?>

<?php
include 'dist/parts/footer.php';
?>
