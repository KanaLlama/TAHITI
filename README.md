# Projet T.A.H.I.T.I.
### It's a magical Place

![](/Ressources/magical.gif)



 Nous avons utilisé VMWARE et une Iso Ubuntu. Pour l’installation et la mise en place de MySQL et Php My Admin sur la VM nous avons suivi la démarche suivante. Nous nous sommes aidés de cette documentation : https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-16-04 .  Nous avons également connecté la base de donnée au serveur et nous récupérons les tables.

#### On vérifie avec ``sudo apt-get update`` que c'est à jour
![](/Ressources/Capture.JPG)

#### Ensuite on installe apache2 grâce à la commande ``sudo apt-get install apache2``
![](/Ressources/Capture.1JPG.JPG)



![](/Ressources/Capture3.JPG)

#### Ici on test la config apache on le redémarre par la suite

![](/Ressources/Capture4.JPG)

#### Ici on vérifie grâce à ``sudo ufw app list`` si mon firewall autorise  HTTPS et HTTP

![](/Ressources/Capture5.JPG)

#### On peut voir ici que le server Apache a bien été installé et qu'il fonctionne

![](/Ressources/Capture6.JPG)

#### On passe alors à l'installation de mysql

![](/Ressources/Capture7.JPG)


![](/Ressources/Capture8.JPG)

#### Ensuite nous installons PHP

![](/Ressources/Capture9.JPG)



![](/Ressources/Capture10.JPG)

![](/Ressources/Capture11.JPG)

#### On remarque ici que PHP est bien installé

![](/Ressources/Capture12.JPG)

![](/Ressources/Capture13.JPG)

#### Ici on installe PhpMyAdmin créer un dossier

![](/Ressources/Capture14.JPG)

#### On recupère l'installe depuis le site officiel

![](/Ressources/Capture15.JPG)

#### Nous avons utilisé la commande ``tar -zxvf phpMyAdmin-4.7.7-all-languages.zip`` mais nous ne l'avons pas screen on dézzip le fichier PhpMyAdmin récupéré.
#### <p> On va dans le dossieer config apache on créer une fichier de configuration pour phpmyadmin </p>

![](/Ressources/Capture16.JPG)

![](/Ressources/Capture19.JPG)

#### On redémarre apache on déplace la config par défault

![](/Ressources/Capture17.JPG)

#### L'installation a reussi nous sommes en version 4.7.7
![](/Ressources/Capture18.JPG)
